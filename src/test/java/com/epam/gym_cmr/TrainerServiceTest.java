package com.epam.gym_cmr;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.epam.gym_cmr.dao.interfaces.TrainerDao;
import com.epam.gym_cmr.dao.interfaces.UserDao;
import com.epam.gym_cmr.model.Trainer;
import com.epam.gym_cmr.model.User;
import com.epam.gym_cmr.services.TrainerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TrainerServiceTest {

    @Mock
    private TrainerDao trainerDao;

    @Mock
    private UserDao userDao;

    @InjectMocks
    private TrainerService trainerService;

    @Test
    public void testCreateTrainerProfile() {
        Trainer trainer = new Trainer();
        User user = new User();
        user.setFirstName("Jane");
        user.setLastName("Smith");

        when(userDao.getUserByUsername(anyString())).thenReturn(null);
        when(trainerDao.createTrainer(trainer)).thenReturn(trainer);

        Trainer createdTrainer = trainerService.createTrainerProfile(trainer);

        assertNotNull(createdTrainer);
        assertNotNull(createdTrainer.getUser().getUsername());
        assertNotNull(createdTrainer.getUser().getPassword());
        assertEquals("Jane.Smith", createdTrainer.getUser().getUsername());
    }

}
