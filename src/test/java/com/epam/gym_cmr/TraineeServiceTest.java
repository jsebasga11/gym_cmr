package com.epam.gym_cmr;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

import com.epam.gym_cmr.dao.interfaces.TraineeDao;
import com.epam.gym_cmr.dao.interfaces.UserDao;
import com.epam.gym_cmr.model.Trainee;
import com.epam.gym_cmr.model.User;
import com.epam.gym_cmr.services.TraineeService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TraineeServiceTest {

    @Mock
    private TraineeDao traineeDao;

    @Mock
    private UserDao userDao;

    @InjectMocks
    private TraineeService traineeService;

    @Test
    public void testCreateTraineeProfile() {
        Trainee trainee = new Trainee();
        User user = new User();
        user.setFirstName("John");
        user.setLastName("Doe");

        when(userDao.getUserByUsername(anyString())).thenReturn(null);
        when(traineeDao.createTrainee(trainee)).thenReturn(trainee);

        Trainee createdTrainee = traineeService.createTraineeProfile(trainee);

        assertNotNull(createdTrainee);
        assertNotNull(createdTrainee.getUser().getUsername());
        assertNotNull(createdTrainee.getUser().getPassword());
        assertEquals("John.Doe", createdTrainee.getUser().getUsername());
    }

}
