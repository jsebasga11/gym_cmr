package com.epam.gym_cmr.services;

import com.epam.gym_cmr.dao.interfaces.TrainingDao;
import com.epam.gym_cmr.model.Training;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrainingService {
    private final TrainingDao trainingDao;

    @Autowired
    public TrainingService(TrainingDao trainingDao) {
        this.trainingDao = trainingDao;
    }

    public Training createTraining(Training training) {
        return trainingDao.createTraining(training);
    }

    public Training getTrainingById(int trainingId) {
        return trainingDao.getTrainingById(trainingId);
    }
}
