package com.epam.gym_cmr.services;

import com.epam.gym_cmr.dao.interfaces.TraineeDao;
import com.epam.gym_cmr.dao.interfaces.UserDao;
import com.epam.gym_cmr.model.Trainee;
import com.epam.gym_cmr.utils.PasswordGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TraineeService {
    private final TraineeDao traineeDao;
    private final UserDao userDao;

    @Autowired
    public TraineeService(TraineeDao traineeDao, UserDao userDao) {
        this.traineeDao = traineeDao;
        this.userDao = userDao;
    }

    public Trainee createTraineeProfile(Trainee trainee) {
        String username = calculateUsername(trainee.getUser().getFirstName(), trainee.getUser().getLastName());
        String password = PasswordGenerator.generatePassword(10);
        trainee.getUser().setUsername(username);
        trainee.getUser().setPassword(password);
        return traineeDao.createTrainee(trainee);
    }

    private String calculateUsername(String firstName, String lastName) {
        String baseUsername = firstName + "." + lastName;
        String username = baseUsername;
        int counter = 1;
        while (userDao.getUserByUsername(username) != null) {
            username = baseUsername + counter;
            counter++;
        }
        return username;
    }
}