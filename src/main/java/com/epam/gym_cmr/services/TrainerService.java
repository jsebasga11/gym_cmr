package com.epam.gym_cmr.services;

import com.epam.gym_cmr.dao.interfaces.TrainerDao;
import com.epam.gym_cmr.dao.interfaces.UserDao;
import com.epam.gym_cmr.model.Trainer;
import com.epam.gym_cmr.utils.PasswordGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrainerService {
    private final TrainerDao trainerDao;
    private final UserDao userDao;

    @Autowired
    public TrainerService(TrainerDao trainerDao, UserDao userDao) {
        this.trainerDao = trainerDao;
        this.userDao = userDao;
    }

    public Trainer createTrainerProfile(Trainer trainer) {
        String username = calculateUsername(trainer.getUser().getFirstName(), trainer.getUser().getLastName());
        String password = PasswordGenerator.generatePassword(10);
        trainer.getUser().setUsername(username);
        trainer.getUser().setPassword(password);
        return trainerDao.createTrainer(trainer);
    }

    private String calculateUsername(String firstName, String lastName) {
        String baseUsername = firstName + "." + lastName;
        String username = baseUsername;
        int counter = 1;
        while (userDao.getUserByUsername(username) != null) {
            username = baseUsername + counter;
            counter++;
        }
        return username;
    }
}
