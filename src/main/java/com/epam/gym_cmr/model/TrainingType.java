package com.epam.gym_cmr.model;

public class TrainingType {
    private String trainingTypeName;

    public String getTrainingTypeName() {
        return trainingTypeName;
    }

    public void setTrainingTypeName(String trainingTypeName) {
        this.trainingTypeName = trainingTypeName;
    }
}
