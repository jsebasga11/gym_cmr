package com.epam.gym_cmr.configuration;

import com.epam.gym_cmr.model.Trainee;
import com.epam.gym_cmr.model.Trainer;
import com.epam.gym_cmr.model.Training;
import com.epam.gym_cmr.model.User;
import com.epam.gym_cmr.dao.impl.TraineeDaoImpl;
import com.epam.gym_cmr.dao.impl.TrainerDaoImpl;
import com.epam.gym_cmr.dao.impl.TrainingDaoImpl;
import com.epam.gym_cmr.dao.impl.UserDaoImpl;
import com.epam.gym_cmr.dao.interfaces.TraineeDao;
import com.epam.gym_cmr.dao.interfaces.TrainerDao;
import com.epam.gym_cmr.dao.interfaces.TrainingDao;
import com.epam.gym_cmr.dao.interfaces.UserDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class AppConfig {
    @Bean
    public Map<String, User> userStorage() {
        return new HashMap<>();
    }

    @Bean
    public Map<Integer, Trainer> trainerStorage() {
        return new HashMap<>();
    }

    @Bean
    public Map<Integer, Trainee> traineeStorage() {
        return new HashMap<>();
    }

    @Bean
    public Map<Integer, Training> trainingStorage() {
        return new HashMap<>();
    }

    @Bean
    public UserDao userDao(Map<String, User> userStorage) {
        return new UserDaoImpl(userStorage);
    }

    @Bean
    public TrainerDao trainerDao(Map<Integer, Trainer> trainerStorage) {
        return new TrainerDaoImpl(trainerStorage);
    }

    @Bean
    public TraineeDao traineeDao(Map<Integer, Trainee> traineeStorage) {
        return new TraineeDaoImpl(traineeStorage);
    }

    @Bean
    public TrainingDao trainingDao(Map<Integer, Training> trainingStorage) {
        return new TrainingDaoImpl(trainingStorage);
    }
}
