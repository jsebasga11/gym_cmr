package com.epam.gym_cmr.configuration;

import com.epam.gym_cmr.model.User;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class StorageConfig {

    @Value("${data.file.path}")
    private String dataFilePath;

    @Bean
    public Map<String, User> userStorage() {
        return new HashMap<>();
    }

    @PostConstruct
    public void initializeStorage() {
        try (BufferedReader reader = new BufferedReader(new FileReader(dataFilePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] fields = line.split(",");
                User user = createUserFromCsv(fields);
                userStorage().put(user.getUsername(), user);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private User createUserFromCsv(String[] fields) {
        // Crear un nuevo objeto User y establecer sus atributos
        User user = new User();
        user.setFirstName(fields[0]);
        user.setLastName(fields[1]);
        user.setUsername(fields[2]);
        user.setPassword(fields[3]);
        user.setActive(Boolean.parseBoolean(fields[4]));
        return user;
    }
}
