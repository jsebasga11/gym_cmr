package com.epam.gym_cmr.dao.impl;

import com.epam.gym_cmr.model.User;
import com.epam.gym_cmr.dao.interfaces.UserDao;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class UserDaoImpl implements UserDao {
    private final Map<String, User> storage;

    public UserDaoImpl(Map<String, User> storage) {
        this.storage = storage;
    }

    @Override
    public User createUser(User user) {
        storage.put(user.getUsername(), user);
        return user;
    }

    @Override
    public User updateUser(User user) {
        if (storage.containsKey(user.getUsername())) {
            storage.put(user.getUsername(), user);
            return user;
        }
        return null; // User not found
    }

    @Override
    public User getUserByUsername(String username) {
        return storage.get(username);
    }

    @Override
    public void deleteUser(String username) {
        storage.remove(username);
    }
}
