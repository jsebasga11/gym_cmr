package com.epam.gym_cmr.dao.impl;

import com.epam.gym_cmr.model.Trainee;
import com.epam.gym_cmr.dao.interfaces.TraineeDao;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class TraineeDaoImpl implements TraineeDao {
    private final Map<Integer, Trainee> storage;

    public TraineeDaoImpl(Map<Integer, Trainee> storage) {
        this.storage = storage;
    }

    @Override
    public Trainee createTrainee(Trainee trainee) {
        int userId = trainee.getUser().getId();
        storage.put(userId, trainee);
        return trainee;
    }

    @Override
    public Trainee updateTrainee(Trainee trainee) {
        int userId = trainee.getUser().getId();
        if (storage.containsKey(userId)) {
            storage.put(userId, trainee);
            return trainee;
        }
        return null; // Trainee not found
    }

    @Override
    public Trainee getTraineeById(int userId) {
        return storage.get(userId);
    }
}

