package com.epam.gym_cmr.dao.impl;

import com.epam.gym_cmr.model.Training;
import com.epam.gym_cmr.dao.interfaces.TrainingDao;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class TrainingDaoImpl implements TrainingDao {
    private Map<Integer, Training> storage;
    private AtomicInteger idCounter;

    public TrainingDaoImpl(Map<Integer, Training> storage) {
        this.storage = storage;
        this.idCounter = new AtomicInteger(0);
    }

    @Override
    public Training createTraining(Training training) {
        int trainingId = idCounter.incrementAndGet();
        training.setId(trainingId);
        storage.put(trainingId, training);
        return training;
    }

    @Override
    public Training getTrainingById(int trainingId) {
        return storage.get(trainingId);
    }

}


