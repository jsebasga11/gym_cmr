package com.epam.gym_cmr.dao.impl;

import com.epam.gym_cmr.model.Trainer;
import com.epam.gym_cmr.dao.interfaces.TrainerDao;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class TrainerDaoImpl implements TrainerDao {
    private final Map<Integer, Trainer> storage;

    public TrainerDaoImpl(Map<Integer, Trainer> storage) {
        this.storage = storage;
    }

    @Override
    public Trainer createTrainer(Trainer trainer) {
        int userId = trainer.getUser().getId();
        storage.put(userId, trainer);
        return trainer;
    }

    @Override
    public Trainer updateTrainer(Trainer trainer) {
        int userId = trainer.getUser().getId();
        if (storage.containsKey(userId)) {
            storage.put(userId, trainer);
            return trainer;
        }
        return null; // Trainer not found
    }

    @Override
    public Trainer getTrainerById(int userId) {
        return storage.get(userId);
    }
}

