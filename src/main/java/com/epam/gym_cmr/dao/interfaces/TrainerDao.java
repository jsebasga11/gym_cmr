package com.epam.gym_cmr.dao.interfaces;

import com.epam.gym_cmr.model.Trainer;

public interface TrainerDao {
    Trainer createTrainer(Trainer trainer);
    Trainer updateTrainer(Trainer trainer);
    Trainer getTrainerById(int userId);
}
