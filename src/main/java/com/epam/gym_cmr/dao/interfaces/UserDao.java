package com.epam.gym_cmr.dao.interfaces;

import com.epam.gym_cmr.model.User;

public interface UserDao {
    User createUser(User user);
    User updateUser(User user);
    User getUserByUsername(String username);
    void deleteUser(String username);
}
