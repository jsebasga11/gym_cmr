package com.epam.gym_cmr.dao.interfaces;

import com.epam.gym_cmr.model.Trainee;

public interface TraineeDao {
    Trainee createTrainee(Trainee trainee);
    Trainee updateTrainee(Trainee trainee);
    Trainee getTraineeById(int userId);
}
