package com.epam.gym_cmr.dao.interfaces;


import com.epam.gym_cmr.model.Training;

public interface TrainingDao {
    Training createTraining(Training training);
    Training getTrainingById(int trainingId);
}
