package com.epam.gym_cmr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GymCmrApplication {

	public static void main(String[] args) {
		SpringApplication.run(GymCmrApplication.class, args);
	}

}
